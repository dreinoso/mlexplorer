package com.chainreaction.mlexplorer.ui.adapters;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chainreaction.mlexplorer.R;
import com.chainreaction.mlexplorer.service.LoadImageTask;
import com.chainreaction.mlexplorer.service.product.Product;
import com.chainreaction.mlexplorer.ui.activities.ProductActivity;

import java.util.ArrayList;
import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    private List<Product> products;

    public ProductsAdapter() {
        products = new ArrayList<>();
    }

    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.products_list, viewGroup, false);
        return new ProductViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder productViewHolder, int index) {
        productViewHolder.product = products.get(index);
        productViewHolder.productTitle.setText(products.get(index).title);
        String priceAndCurrency = products.get(index).price + " " +
                products.get(index).currencyId;
        productViewHolder.productPrice.setText(priceAndCurrency);
        new LoadImageTask(productViewHolder.thumbnailImage).execute(products.get(index).thumbnail);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setDataAndNotify(ArrayList<Product> newProducts) {
        products = newProducts;
        notifyDataSetChanged();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnailImage;
        public TextView productTitle;
        public TextView productPrice;
        public ProductsAdapter adapter;
        public Product product;

        public ProductViewHolder(View view, ProductsAdapter adapter) {
            super(view);
            thumbnailImage = view.findViewById(R.id.thumbnail_view);
            productTitle = view.findViewById(R.id.product_title);
            productPrice = view.findViewById(R.id.product_price);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lunchProductActivity(view);
                }
            });
            this.adapter = adapter;
        }

        private void lunchProductActivity(View view) {
            Intent intent = new Intent(view.getContext(), ProductActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(view.getContext().getString(R.string.intent_product_key), product);
            intent.putExtras(bundle);
            view.getContext().startActivity(intent);
        }
    }
}