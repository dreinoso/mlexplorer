package com.chainreaction.mlexplorer.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.chainreaction.mlexplorer.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        dismissSplash();
    }

    private void dismissSplash() {
        final int splashDuration = 2000;
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent toRadioActivity= new Intent(getBaseContext(), MainActivity.class);
                startActivity(toRadioActivity);
                finish();
            }
        }, splashDuration);
    }
}
