package com.chainreaction.mlexplorer.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chainreaction.mlexplorer.R;
import com.chainreaction.mlexplorer.service.RequestProductTask;
import com.chainreaction.mlexplorer.service.product.Product;
import com.chainreaction.mlexplorer.ui.adapters.ImagesAdapter;
import com.chainreaction.mlexplorer.ui.interfaces.RequestProductListener;

public class ProductActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getName();

    private Product product;
    ImagesAdapter imagesAdapter;
    private ViewPager imagesPager;
    private TextView productDescription;
    private TextView productCondition;
    private TextView productPrice;
    private Button buyButton;

    private RequestProductListener requestProductListener = new RequestProductListener() {
        @Override
        public void onRequestFinished(Product productWithPictures) {
            Log.d(TAG,"onRequestFinished product.images: " + product.pictures);
            imagesAdapter = new ImagesAdapter(getApplicationContext(),product.pictures);
            imagesPager.setAdapter(imagesAdapter);
            product = productWithPictures;
            productDescription.setText(product.title);
            if (product.condition != null) {
                productCondition.setText(product.condition);
            }
            String priceAndCurrency = product.price + " " + product.currencyId;
            productPrice.setText(priceAndCurrency);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        linkViews();
        loadProduct();
        setBuyButtonClick();
    }

    private void linkViews() {
        imagesPager = findViewById(R.id.images_pager);
        productDescription = findViewById(R.id.product_description_text);
        productCondition = findViewById(R.id.single_product_condition);
        productPrice = findViewById(R.id.single_product_price);
        buyButton = findViewById(R.id.buy_button);
    }

    private void loadProduct() {
        Intent sourceIntent = getIntent();
        Bundle state = sourceIntent.getExtras();
        if (state != null) {
            product = (Product) state.getSerializable(getString(R.string.intent_product_key));
            Log.d(TAG, "searchProduct: " + product.title + " " + product.price + " " + product.permalink);
            new RequestProductTask(requestProductListener).execute(product);
        }
    }

    private void setBuyButtonClick() {
        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent buyAction = new Intent(Intent.ACTION_VIEW);
                buyAction.setData(Uri.parse(product.permalink));
                startActivity(buyAction);
            }
        });
    }
}
