package com.chainreaction.mlexplorer.ui.interfaces;

import com.chainreaction.mlexplorer.service.product.Product;

import java.util.ArrayList;

public interface RequestProductListener {

    void onRequestFinished(Product products);

}
