package com.chainreaction.mlexplorer.ui.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.chainreaction.mlexplorer.service.LoadImageTask;

import java.util.ArrayList;

public class ImagesAdapter extends PagerAdapter {

    Context context;
    private ArrayList<String> picturesUrls;

    public ImagesAdapter(Context newContext, ArrayList<String> newPicturesUrls) {
        context = newContext;
        picturesUrls = newPicturesUrls;
    }

    @Override
    public int getCount() {
        return picturesUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        new LoadImageTask(imageView).execute(picturesUrls.get(position));
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}