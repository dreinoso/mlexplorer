package com.chainreaction.mlexplorer.ui.activities;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.SearchView;
import android.widget.Toast;

import com.chainreaction.mlexplorer.R;
import com.chainreaction.mlexplorer.service.SearchProductsTask;
import com.chainreaction.mlexplorer.service.product.Product;
import com.chainreaction.mlexplorer.ui.adapters.ProductsAdapter;
import com.chainreaction.mlexplorer.ui.interfaces.SearchProductsListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getName();

    private SearchView searchView;
    private RecyclerView productsView;
    private ProductsAdapter productsAdapter;

    private SearchProductsListener productsListener = new SearchProductsListener() {
        @Override
        public void onSearchFinished(ArrayList<Product> products) {
            productsAdapter.setDataAndNotify(products);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSearchView();
        productsAdapter = new ProductsAdapter();
        setRecyclerView();
    }

    private void setSearchView() {
        searchView = findViewById(R.id.search_view);
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String product) {
                Log.d(TAG, "onQueryTextSubmit: " + product);
                boolean searchSuccessful = false;
                if (isNetworkAvailable()) {
                    searchProduct(product);
                    searchView.clearFocus();
                    searchSuccessful = true;
                } else {
                    showNoInternetToast();
                }
                return searchSuccessful;
            }

            private boolean isNetworkAvailable() {
                ConnectivityManager connectivityManager
                        = (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnected();
            }

            private void showNoInternetToast() {
                Toast toast = Toast.makeText(getApplicationContext(),
                        R.string.no_internet_connection,
                        Toast.LENGTH_LONG);
                toast.show();
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    private void searchProduct(String product) {
        Log.d(TAG, "searchProduct: " + product);
        new SearchProductsTask(productsListener).execute(product);
    }

    private void setRecyclerView() {
        productsView = findViewById(R.id.products_view);
        productsView.setHasFixedSize(true);
        productsView.setLayoutManager(new LinearLayoutManager(this));
        productsView.setItemAnimator(new DefaultItemAnimator());
        productsView.setAdapter(productsAdapter);
    }

    @Override
    public void onBackPressed() {
        goHomeInsteadOfPreviousActivity();
    }

    private void goHomeInsteadOfPreviousActivity() {
        Intent goHomeIntent = new Intent(Intent.ACTION_MAIN);
        goHomeIntent.addCategory(Intent.CATEGORY_HOME);
        goHomeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(goHomeIntent);
    }
}
