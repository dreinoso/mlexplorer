package com.chainreaction.mlexplorer.service.product;

public class ProductBuilder {
    private String id;
    private String title;
    private String permalink;
    private String thumbnail;
    private String price;
    private String currencyId;
    private String condition;

    public Product build() {
        Product product = new Product();

        product.id = id;
        product.title = title;
        product.permalink = permalink;
        product.thumbnail = thumbnail;
        product.price = price;
        product.currencyId = currencyId;
        product.condition = condition;

        return product;
    }


    public ProductBuilder setId(String newId) {
        id = newId;
        return this;
    }

    public ProductBuilder setTitle(String newTitle) {
        title = newTitle;
        return this;
    }

    public ProductBuilder setPermaLink(String newPermalink) {
        permalink = newPermalink;
        return this;
    }

    public ProductBuilder setThumbnail(String newThumbnail) {
        thumbnail = newThumbnail;
        return this;
    }

    public ProductBuilder setPrice(String newPrice) {
        price = newPrice;
        return this;
    }

    public ProductBuilder setCurrencyId(String newCurrencyId) {
        currencyId = newCurrencyId;
        return this;
    }

    public ProductBuilder setCondition(String newCondition) {
        condition = newCondition;
        return this;
    }
}
