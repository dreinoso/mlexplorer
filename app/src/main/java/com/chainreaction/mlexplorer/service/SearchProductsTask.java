package com.chainreaction.mlexplorer.service;

import android.os.AsyncTask;
import android.util.Log;

import com.chainreaction.mlexplorer.service.product.Product;
import com.chainreaction.mlexplorer.service.product.ProductBuilder;
import com.chainreaction.mlexplorer.ui.interfaces.SearchProductsListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.*;

public class SearchProductsTask extends AsyncTask<String, String, ArrayList<Product>> {

    private final String TAG = this.getClass().getName();
    private final String REQUEST_WEB_PREFIX = "https://api.mercadolibre.com/sites/MLA/search?q=";
    private SearchProductsListener listener;

    public SearchProductsTask(SearchProductsListener newListener) {
        listener = newListener;
    }

    @Override
    protected ArrayList<Product> doInBackground(String... params) {
        StringBuffer response = new StringBuffer();
        ArrayList<Product> products = new ArrayList<>();
        try {
            String product = params[0];
            String request = REQUEST_WEB_PREFIX + product;
            Log.d(TAG, "doInBackground request: " + request);
            URL urlForGetRequest = new URL(request);
            String readLine = null;
            HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                while ((readLine = in.readLine()) != null) {
                    response.append(readLine);
                    products = parseJson(response);
                }
                in.close();
                Log.d(TAG, "JSON String Result " + response.toString());
            } else {
                System.out.println("GET NOT WORKED");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    private ArrayList<Product> parseJson(StringBuffer response) {
        ArrayList<Product> productsList = new ArrayList();
        JSONObject obj = null;

        try {
            obj = new JSONObject(response.toString());
            // todo dislpay totalProducts at top of results
            String totalProducts = obj.getJSONObject("paging").getString("total");
            JSONArray products = obj.getJSONArray("results");

            ProductBuilder builder = new ProductBuilder();
            for (int i = 0; i < products.length(); i++)
            {
                Product prod = builder.setId(products.getJSONObject(i).getString("id")).
                        setTitle(products.getJSONObject(i).getString("title")).
                        setPermaLink(products.getJSONObject(i).getString("permalink")).
                        setThumbnail(products.getJSONObject(i).getString("thumbnail")).
                        setPrice(products.getJSONObject(i).getString("price")).
                        setCurrencyId(products.getJSONObject(i).getString("currency_id")).
                        setCondition(products.getJSONObject(i).getString("condition")).
                        build();

                productsList.add(prod);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "parseJson productsList: " + productsList );
        return productsList;
    }

    @Override
    protected void onPostExecute(ArrayList<Product> products) {
        super.onPostExecute(products);
        listener.onSearchFinished(products);
    }
}
