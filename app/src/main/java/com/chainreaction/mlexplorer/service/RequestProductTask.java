package com.chainreaction.mlexplorer.service;

import android.os.AsyncTask;
import android.util.Log;

import com.chainreaction.mlexplorer.service.product.Product;
import com.chainreaction.mlexplorer.ui.interfaces.RequestProductListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class RequestProductTask extends AsyncTask<Product, String, Product> {

    private final String TAG = this.getClass().getName();
    private final String REQUEST_WEB_PREFIX = "https://api.mercadolibre.com/items/";
    private RequestProductListener listener;

    public RequestProductTask(RequestProductListener newListener) {
        listener = newListener;
    }

    @Override
    protected Product doInBackground(Product... params) {
        Product product = params[0];
        StringBuffer response = new StringBuffer();
        try {
            String request = REQUEST_WEB_PREFIX + product.id;
            Log.d(TAG, "doInBackground request: " + request);
            URL urlForGetRequest = new URL(request);
            String readLine;
            HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                while ((readLine = in.readLine()) != null) {
                    response.append(readLine);
                }
                in.close();
                product.pictures = getPicturesFromJson(response);
                Log.d(TAG, "JSON String Result " + response.toString());
            } else {
                System.out.println("GET NOT WORKED");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return product;
    }

    private ArrayList<String> getPicturesFromJson(StringBuffer response) {
        ArrayList<String> pictures = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(response.toString());
            JSONArray products = obj.getJSONArray("pictures");

            for (int i = 0; i < products.length(); i++)
            {
                pictures.add(products.getJSONObject(i).getString("url"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "getPicturesFromJson pictures: " + pictures);
        return pictures;
    }

    @Override
    protected void onPostExecute(Product product) {
        super.onPostExecute(product);
        listener.onRequestFinished(product);
    }
}
