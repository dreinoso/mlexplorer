package com.chainreaction.mlexplorer.service.product;

import java.io.Serializable;
import java.util.ArrayList;

public class Product implements Serializable {

    public String id;
    public String title;
    public String permalink;
    public String thumbnail;
    public String price;
    public String currencyId;
    public String condition;
    public ArrayList<String> pictures;

    public Product(String newId,
                   String newTitle,
                   String newPermalink,
                   String newThumbnail,
                   String newPrice,
                   String newCurrencyId,
                   String newCondition) {

        id = newId;
        title = newTitle;
        permalink = newPermalink;
        thumbnail = newThumbnail;
        price = newPrice;
        currencyId = newCurrencyId;
        condition = newCondition;
    }

    public Product() { }
}
