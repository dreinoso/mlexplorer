package com.chainreaction.mlexplorer.service;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

public class LoadImageTask extends AsyncTask<String, String, Bitmap> {
    ImageView img=null;

    public LoadImageTask(ImageView img){
        this.img=img;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Bitmap doInBackground(String... args) {
        Bitmap bitmap=null;
        try {
            bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap image) {
        if(image != null){
            img.setImageBitmap(image);
        }
    }
}